import React, { Component } from "react";
import UserService from "../services/user.service";
import "./test.css";
import {Link } from "react-router-dom";
import Img1 from './img2.jpg';
import Img4 from './img4.jpg';
import Img3 from './img3.jpg';
import Img5 from './img5.jpg';
import Img6 from './img1.jpg';
import Img7 from './img7.jpg';
import Img8 from './img8.jpg';
import Img9 from './img9.jpg';
import Img2 from './creation.PNG';


export default class Presentation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: ""
    };
    this.modal = document.getElementById('myModal');
    this.img = document.getElementById('myImg');
    this.captionText = document.getElementById("caption");
    this.modalImg = document.getElementById("img01");
  }

  componentDidMount() {
    UserService.getPublicContent().then(
      response => {
        this.setState({
          content: response.data
        });
      },
      error => {
        this.setState({
          content:
            (error.response && error.response.data) ||
            error.message ||
            error.toString()
        });
      }
    );
  }

  render() {
    return (
      <div className="container">
        <header className="jumbotron">
          <h2>Présentation</h2>
          <p>Voici quelques créations que nous avons réaliser, N'hésitez pas à nous <Link to={"/contact"} className="redirection">
              <strong>contacter</strong></Link> pour tous renseignement.</p>
          <div className="conteneur">
            <div className="cont" data-aos="fade-left">
              <img src={Img1} id="img" class ="image" alt="image1" />
              <div class="overlay">
                <p class="text">Signalétique</p>
                <p class ="des">Adhésif pour l'hôtel de ville de Brest</p>
              </div>
            </div>
            <div className="cont" data-aos="fade-up">
                <img src={Img3}  id="img" class ="image" alt="image2" />
              <div class="overlay">
                <p class="text">Vitrophanie</p>
                <p class="des">Pose de dépoli au Bar / Restaurant le 6, du Leclerc de Kergaradec</p>
              </div>
            </div>
            <div className="cont" data-aos="fade-right">
              <img src={Img4} id="img" class ="image" alt="image3" />
              <div class="overlay">
                <p class="text">Enseignes</p>
                <p class="des">Totem de consignes d'accueil du magasin de coiffure New Look Jean jaurès à Brest</p>
              </div>
            </div>
            <div className="cont" data-aos="fade-up">
              <img src={Img5} id="img" class ="image" alt="image10" />
              <div class="overlay">
                <p class="text">Support De Communication</p>
                <p class="des">Lettres découpées adhésives</p>
              </div>
            </div>
            <div className="cont" data-aos="fade-up">
              <img src={Img8} id="img" class ="image" alt="image5" />
              <div class="overlay">
                <p class="text">Adhesif</p>
                <p class="des">Découpe et pose de lettres découpées, pour la société BMI, spécialisée en mécanique industrie</p>
              </div>
            </div>
            <div className="cont" data-aos="fade-up">
              <img src={Img7} id="img" class ="image" alt="image6" />
              <div class="overlay">
                <p class="text">Impressions Diverses</p>
                <p class ="des">Pour un effet du Tonnerre ! Vous voulez décorer une salle avec vos joueurs ou stars préférés en grandeur nature ?!! Voici Mickael Jordan en silhouette !</p>
              </div>
            </div>
            <div className="cont" data-aos="fade-up">
              <img src={Img9} id="img" class ="image" alt="image7" />
              <div class="overlay">
                <p class="text">Textile</p>
                <p class="des">Flocage de p'tits tee-shirts sympa pour le festival ElffFest !!</p>
              </div>
            </div>
            <div className="cont" data-aos="fade-up" >
              <img src={Img6} id="img" class ="image" alt="image8" />
              <div class="overlay">
              <p class="text">Marquage Véhicule</p>
              <p class="des">Marquage du camion EMTH : Entreprise générale du bâtiment</p>
              </div>
            </div>
            <div className="cont" data-aos="fade-up">
              <img src={Img2} id="img" class ="image" alt="image9" />
              <div class="overlay">
                <p class="text">Création</p>
                <p class="des">Voici une idée cadeau pour les Fans des Albatros ! Une Horloge d'un de leurs joueurs ! N'hésitez pas à vous renseigner !!</p>
              </div>
            </div>
          </div>
        </header>
        <footer className="mb-4">
        <div><a href="https://www.facebook.com/iledelimpression"><i class="fa fa-facebook-official"/></a></div>
        <p>Tous droits réservés - 2021 - île de l'impression. Reproductions des images et textes interdites.<br/>
        Designed & developped by <strong><a href="https://www.facebook.com/salmane.chahidi.56/">SC</a></strong></p>
        </footer>
      </div>
    );
  }
}
