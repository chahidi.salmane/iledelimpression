import React, { Component } from "react";

import UserService from "../services/user.service";
import Img1 from './image1.jpg';
import {Link } from "react-router-dom";

export default class DescriptionImage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: ""
    };
  }

  componentDidMount() {
    UserService.getPublicContent().then(
      response => {
        this.setState({
          content: response.data
        });
      },
      error => {
        this.setState({
          content:
            (error.response && error.response.data) ||
            error.message ||
            error.toString()
        });
      }
    );
  }

  render() {
    return (
      <div className="container">
        <header className="jumbotron">
          <div className="back">
            <Link to={"/presentation"} >Retour<i class="fa fa-arrow-left"></i></Link>
          </div>
          <h2><img src={Img1} height="450" width="324px" alt="image1" /></h2>
          <p>De la création de maquette et la fabrication à la pose d'enseignes, nous pouvons vous proposer une large gamme de produits et de supports.
- Notre point fort est de tout réaliser dans notre atelier. Pour tout renseignement et étude précise, n'hésitez pas à nous contacter :
par téléphone au 02 98 42 20 41
ou par mail : iledelimpression@orange.fr
- Véritable carte d’identité de votre commerce, l’enseigne est primordiale pour être vu, connu et reconnu.
- De la conception de la maquette à la pose, nous réalisons tous types d’enseignes dans des délais rapides.</p>
        </header>
        <footer className="mb-4">
        <div><a href="https://www.facebook.com/iledelimpression"><i class="fa fa-facebook-official"/></a></div>
        <p>Tous droits réservés - 2021 - île de l'impression. Reproductions des images et textes interdites.<br/>
        Designed & developped by <strong><a href="https://www.facebook.com/salmane.chahidi.56/">SC</a></strong></p>
        </footer>
      </div>
    );
  }
}