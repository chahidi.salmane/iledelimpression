import { Component } from "react";
import UserService from "../services/user.service";
import valider from './valider.gif';

export default class Message extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content:'',
      name: '',
      email: '',
      subject: '',
      message: '',
    }
  }

  componentDidMount() {
    UserService.getPublicContent().then(
      response => {
        this.setState({
          content: response.data
        });
      },
      error => {
        this.setState({
          content:
            (error.response && error.response.data) ||
            error.message ||
            error.toString(),
        });
      }
    );
  }

  render() {
    return (
      <div className="container">
        <header className="jumbotron">
            <img src={valider} alt="image2" />
            <p>Message envoyé !</p>
        </header>
      </div>
    );
  }
}