import { Component } from "react";
import emailjs from "emailjs-com";
import { Redirect } from "react-router-dom";
import UserService from "../services/user.service";
import Img4 from './image4.PNG';

export default class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content:'',
      name: '',
      email: '',
      subject: '',
      message: '',
      valider :false
    }
    this.sendEmail = this.sendEmail.bind(this);
  }
 
  sendEmail(e) {
    e.preventDefault();
    this.setState({
      valider:true,
    });
    emailjs.sendForm('service_ix60whq', 'template_y2xozm7', e.target, 'user_IVJRiMbdPqRYscAhJVjT8')
      .then((result) => {
          console.log(result.text);
      }, (error) => {
          console.log(error.text);
      });
      e.target.reset();
        
  }

  componentDidMount() {
    UserService.getPublicContent().then(
      response => {
        this.setState({
          content: response.data
        });
      },
      error => {
        this.setState({
          content:
            (error.response && error.response.data) ||
            error.message ||
            error.toString(),
        });
      }
    );
  }

  render() {
    const { valider } = this.state;
    return (
      <div className="container">
        <header className="jumbotron">
          <h2>Contact</h2>
          <p>Si vous souhaitez avoir des informations sur île de l'impression, pour des commandes liées à votre entreprise, 
            ainsi que pour tout autre demande n’hésitez pas à nous contacter !
            On est à votre disposition pour vous éclairer sur nos propositions.</p>
          <div className="contact-form" >
            <div  data-aos="fade-right">
            <img src={Img4} class ="image4" id="contactbloc" alt="image4"/>
            </div >
            <div data-aos="fade-left">
            <form class="formm" id="contactbloc" onSubmit={this.sendEmail}>
              <p className="contactUs" id="contactUs">Contactez-nous</p>
              <p type="Votre nom (Obligatoire)"><input type="text" name="name" required></input></p>
              <p type="Votre email (Obligatoire)"><input type="email" name="email" required></input></p>
              <p type="Sujet"><input name="subject" ></input></p>
              <p type="Votre message"><textarea name="message" required/></p>
              <button type="submit">Envoyer</button>
              {valider && (
              <Redirect to="/valider" />
            )}
              <div className="contact">
                <span className="fa fa-phone"></span>02 98 42 20 41
                <span className="fa fa-envelope-o"> <a href={"mailto:iledelimpression@orange.fr"} id="mail">iledelimpression@orange.fr</a></span>
              </div>
            </form>
            </div>
          </div>
        </header>
        <p className="adresse"><strong>Adresse : </strong>3 Rue Bis Fernand Forest, 29850 Gouesnou</p>
        <footer className="mb-4">
        <div><a href="https://www.facebook.com/iledelimpression"><i class="fa fa-facebook-official"/></a></div>
        <p>Tous droits réservés - 2021 - île de l'impression. Reproductions des images et textes interdites.<br/>
        Designed & developped by <strong><a href="https://www.facebook.com/salmane.chahidi.56/">SC</a></strong></p>
        </footer>
      </div>
    );
  }
}