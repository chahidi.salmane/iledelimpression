import "../App.css";
import {Carousel } from 'react-responsive-carousel';
import img1 from './aurelie.png';
import img2 from './stephanie.png';
import img4 from './Patrick.png';
import img3 from './jerome.jpg';
import img6 from './test1.png';
import img5 from './image.jpg';
import "react-responsive-carousel/lib/styles/carousel.min.css";
const TestimonialsPage = () => {
  return (
    <Carousel className="characterCarousel"
        showArrows={true}
        infiniteLoop={true}
        showThumbs={false}
        showStatus={false}
        autoPlay={true}
        interval={6100}
      >
        <div>
          <img src={img5} alt ="img1"/>
          <div className="myCarousel">
            <h3>Thierry</h3>
            <h4>Directeur</h4>
            <p >"Fondateur de l'île de l'impression"
            </p>
          </div>
        </div>

        <div>
          <img src={img2} alt ="img" />
          <div className="myCarousel">
            <h3>Stéphanie</h3>
            <h4>Imprimeuse</h4>
            <p>"Préparateur des commandes des clients"
            </p>
          </div>
        </div>

        <div>
          <img src={img3} alt ="img"/>
          <div className="myCarousel">
            <h3>Jérôme</h3>
            <h4>Imprimeur</h4>
            <p>"Préparateur des commandes des clients"
            </p>
          </div>
        </div>

        <div>
          <img src={img4} alt ="img"/>
          <div className="myCarousel">
            <h3>Patrick</h3>
            <h4>Graphiste</h4>
            <p>"Résponsable de la communication visuelle "
            </p>
          </div>
        </div>

        <div>
          <img src={img6} alt ="img"/>
          <div className="myCarousel">
            <h3>Xavier</h3>
            <h4>Comptable</h4>
            <p>"Traitement des factures & renseignement clients"
            </p>
          </div>
        </div>

        <div>
          <img src={img1} alt ="img"/>
          <div className="myCarousel">
            <h3>Aurélie</h3>
            <h4>Graphiste & Imprimeuse</h4>
            <p>"Création des graphiques & Préparateur des commandes des clients"
            </p>
          </div>
        </div>
      </Carousel>
  );
};

export default TestimonialsPage;