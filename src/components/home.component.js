import  { Component } from "react";
import {Carousel } from 'react-bootstrap';
import UserService from "../services/user.service";
import Img15 from "./image15.jpg";

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: "",
      accueil:[],
    };
  }

  componentDidMount() {
    fetch("https://www.googleapis.com/drive/v3/files/1dlCf98ipe0K7FGagOtm4WtCwVVLOcsMT?alt=media&key=AIzaSyAuMSwiEqmFM6oxEmItfe9KfUO2kpv5plo",{
      headers : { 
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': '*',

       }
    })
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            image1: result.accueil[0].image,
            titre1: result.accueil[0].titre,
            description1: result.accueil[0].description,
            image2: result.accueil[1].image,
            titre2: result.accueil[1].titre,
            description2: result.accueil[1].description,
            image3: result.accueil[2].image,
            titre3: result.accueil[2].titre,
            description3: result.accueil[2].description,
            image4: result.accueil[3].image,
            titre4: result.accueil[3].titre,
            description4: result.accueil[3].description,
            image5: result.accueil[4].image,
            titre5: result.accueil[4].titre,
            description5: result.accueil[4].description,
            titre: result.accueil[5].contenu[0].titre,
            contentDescription: result.accueil[5].contenu[0].description,
            quote: result.accueil[5].contenu[0].quote,
          });
          console.log(result.accueil);
          console.log(result.accueil[0].titre);
          console.log(result.accueil[0].image);
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
        
      )
    UserService.getPublicContent().then(
      response => {
        this.setState({
          content: response.data
        });
      },
      error => {
        this.setState({
          content:
            (error.response && error.response.data) ||
            error.message ||
            error.toString()
        });
      }
    );
  }

  render() {
            const { image2, image3, image4, image5, titre2, description2, titre3, description3, titre4, description4, titre5, description5 } = this.state;
        return (
          <div>
              <div className="container">
                <Carousel fade className="homeCarousel">
                  <Carousel.Item interval={3000}>
                    <div>
                    <img
                      className="d-block w-100"
                      src={Img15}
                      alt="First slide"
                    />
                    <Carousel.Caption>
                      <h3 className="title-image">Habillage de vitre</h3>
                      <p className="subtitle-image">La jardinerie Lagadec à St Thonan a fait le printemps</p>
                    </Carousel.Caption>
                    </div>
                  </Carousel.Item>
                  <Carousel.Item interval={3000} >
                    <img
                      className="d-block w-100"
                      src={image2}
                      alt="Second slide"
                    />

                    <Carousel.Caption>
                      <h3 className="title-image">{titre2}</h3>
                      <p className="subtitle-image">{description2}</p>
                    </Carousel.Caption>
                  </Carousel.Item>
                  <Carousel.Item interval={3000}>
                    <img
                      className="d-block w-100"
                      src={image3}
                      alt="Third slide"
                    />
                    <Carousel.Caption>
                      <h3 className="title-image">{titre3}</h3>
                      <p className="subtitle-image">{description3}</p>
                    </Carousel.Caption>
                  </Carousel.Item>
                  <Carousel.Item interval={3000}>
                    <img
                      className="d-block w-100"
                      src={image4}
                      alt="Third slide"
                    />
                    <Carousel.Caption>
                      <h3 className="title-image">{titre4}</h3>
                      <p className="subtitle-image">{description4}</p>
                    </Carousel.Caption>
                  </Carousel.Item>
                  <Carousel.Item interval={3000}>
                    <img
                      className="d-block w-100"
                      src={image5}
                      alt="Third slide"
                    />
                    <Carousel.Caption>
                      <h3 className="title-image">{titre5}</h3>
                      <p className="subtitle-image">{description5}</p>
                    </Carousel.Caption>
                  </Carousel.Item>
                </Carousel>
              <header className="jumbotron">  
                <h2>Bienvenue chez l'île de l'impression</h2>
                <p>De la création de maquette et la fabrication à la pose d'enseignes, nous pouvons vous proposer une large gamme de produits et de supports. - Notre point     fortest de tout réaliser dans notre atelier. Pour tout renseignement et étude précise, n'hésitez pas à nous contacter : par téléphone au 02 98 42 20 41 ou par mail : iledelimpression@orange.fr - Véritable carte d’identité de votre commerce, l’enseigne est primordiale pour être vu, connu et reconnu. - De la conception de la maquette à la pose, nous réalisons tous types d’enseignes dans des délais rapides.</p>
                <p className="quote">On imprime tout sur tout et partout !</p>
              </header>
              <footer className="mb-4">
              <div><a href="https://www.facebook.com/iledelimpression"><i class="fa fa-facebook-official"/></a></div>
              <p>Tous droits réservés - 2021 - île de l'impression. Reproductions des images et textes interdites.<br/>
              Designed & developped by <strong><a href="https://www.facebook.com/salmane.chahidi.56/">SC</a></strong></p>
              </footer>
            </div></div>
        );
  }
}