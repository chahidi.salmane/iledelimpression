import React, { Component } from "react";

import UserService from "../services/user.service";
import { Link } from "react-router-dom";
import { Navbar, Nav, Container } from 'react-bootstrap';
import Logo from './logo.jpeg';

export default class BarMenu extends Component {
  constructor(props) {
    super(props);

    this.state = {
        showModeratorBoard: false,
        showAdminBoard: false,
        currentUser: undefined,
        loading: true
      };
  }

  componentDidMount() {
    UserService.getPublicContent().then(
      response => {
        this.setState({
          content: response.data
        });
      },
      error => {
        this.setState({
          content:
            (error.response && error.response.data) ||
            error.message ||
            error.toString()
        });
      }
    );
  }

  render() {
    const { currentUser, showModeratorBoard, showAdminBoard } = this.state;
    return (
        <div>
        <nav className="navbar navbar-expand navbar-light bg-white" id="administration-bar">
          <div className="navbar-nav mr-auto">
            {showModeratorBoard && (
              <li className="nav-item">
                <Link to={"/mod"} className="nav-link">
                  Moderator Board
                </Link>
              </li>
            )}

            {showAdminBoard && (
              <li className="nav-item">
                <Link to={"/admin"} className="nav-link">
                  Admin Board
                </Link>
              </li>
            )}

            {currentUser && (
              <li className="nav-item">
                <Link to={"/user"} className="nav-link">
                  Utilisateur
                </Link>
              </li>
            )}
          </div>

          {currentUser ? (
            <div className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to={"/profile"} className="nav-link">
                  {currentUser.username}
                </Link>
              </li>
              <li className="nav-item">
                <a href="/login" className="nav-link" onClick={this.logOut}>
                  Se déconnecter
                </a>
              </li>
            </div>
          ) : (
            <div className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to={"/login"} className="nav-link" id="login">
                  Se connecter
                </Link>
              </li>

              <li className="nav-item">
                <Link to={"/register"} className="nav-link" id="register">
                  S'inscrire
                </Link>
              </li>
            </div>
          )}
        </nav>
        <div className="container mt-3 ">
        <div className="center-navbar" >
        <Navbar fixed="top" expand="md">
              <Container>
                  <Navbar.Toggle aria-controls='responsive-navbar-nav' />
                  <Navbar.Collapse id='responsive-navbar-nav'>
                    <Nav fixed="top" activeKey={window.location.pathname} >
                      <Nav.Link  id="menu" href='/home' >Accueil</Nav.Link>
                      <Nav.Link id="menu" href='/description' >Qui sommes-nous?</Nav.Link>
                      <Nav.Link href='/'><img src={Logo} id="logohome" alt = "image1"/></Nav.Link>
                      <Nav.Link id="menu" href='/presentation' >Présentation</Nav.Link>
                      <Nav.Link id="menu" href='/contact' >Contact</Nav.Link>
                    </Nav>
                  </Navbar.Collapse>
              </Container>
        </Navbar>
        </div>
        </div>
        </div>
    );
  }
}
