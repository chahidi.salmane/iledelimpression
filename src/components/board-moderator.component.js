import  { Component } from "react";
import axios from "axios";

export default class BoardUser extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: "",
      message: [],
    };
  }

  componentDidMount() {
    return axios.get('http://localhost:8080/users')
    .then(response => {
       this.setState({ message : response.data });
       console.log(response.data);
    });
    
  }
  

  render() {
    return (
      <div className="container">
        <header className="jumbotron">
        <table>
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Username</th>
                    <th>email</th>
                    <th>Date de création</th>
                    <th>Dernière modification</th>
                    <th colSpan="2">Action</th>
                </tr>
            </thead>
            <tbody>{this.state.message.map(item => 
            
                <tr>
                    <td key={item.id}>{item.id}</td>
                    <td key={item.username}>{item.username}</td>
                    <td key={item.email}>{item.email}</td>
                    <td>{item.createdAt}</td>
                    <td>{item.updatedAt}</td>
                    <td colSpan="2">
                      <button>supprimer</button>
                      <button>edit</button>
                    </td>
                </tr>
            
          
        )}</tbody>
        </table>
        </header>
      </div>
    );
  }
}