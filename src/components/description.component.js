import { Component } from "react";
import UserService from "../services/user.service";
import Img1 from './image20.jpg';
import img2 from './image4.PNG'
import TestimonialsPage from "./testimonials.component";


export default class Description extends Component {
  constructor(props) {
    super(props);

    this.state = {
      content: "",
    };
  }

  componentDidMount() {
    UserService.getPublicContent().then(
      response => {
        this.setState({
          content: response.data
        });
      },
      error => {
        this.setState({
          content:
            (error.response && error.response.data) ||
            error.message ||
            error.toString(),
        });
      }
    );
  }

  render() {
    return (
      <div className="container">
        <header className="jumbotron">
          <h2>Qui sommes-nous ?</h2>
          <p>L’entreprise île de l’impression a été fondée en 2006 par Thierry ALLAIN.  Localisée à GOUESNOU (29850), 
            elle est spécialisée dans le secteur d'activité de l'autre imprimerie (labeur). Son effectif est compris entre 3 et 5 salariés. </p>
        </header>
        <div class="row">
          <div class="col">
          <img src={img2} id="imge" alt="image2" height ="450px" width="625px"/>
          <p id="text" class="fade-in">
            Notre savoir faire et nos compétences reposent sur nos personnels expérimentés et motivés.
            Quelque soit le produit souhaité à imprimer, île de l’impression est capable d’imprimé tout surtout et partout.
            Réaliser des prestations de qualité dans une recherche constante de satisfaction du client.
            Notre valeur est de savoir-faire et Qualité, Engagement, Disponibilité, Responsabilité, Confiance et Proximité.
            </p> 
            <p id="text">
            Conception et réalisation des produits imprimés dans le domaine d’imprimerie ainsi qu’à l’export :
          • Neuf, rénovation,
          • Communication,
          • Livraison et installation sur place,
          • Produit haut de gamme, 
          • Meilleur prix du marché,
          • Très bonne réputation,
          • Confiance et fidélité des clients. 
            </p>
          </div>
          <div class="col">
          <p id="text" class="a">
          
          <h3 class="a"> Informations complémentaires </h3>
          <strong>Date de création</strong> : 02-10-2006,
          <strong> Forme juridique</strong> : SARL unipersonnelle, 
          <strong> Effectif</strong> : 5 salariés, 
          <strong> Capital social</strong> : 8000,00 €, 
          <strong> Catégorie</strong> : Industrie bois, papier, carton </p>
            <p></p>
            <p></p>
            <img src={Img1} id="imge" alt="image2" height ="450px" width="625px"/>
            <p>
            <strong>Adresse :</strong> 3 Rue Bis Fernand Forest, 29850 Gouesnou
            </p>
            <p>
            <strong>Téléphone :</strong> 02 98 42 20 41
            </p>
            <p>
            <strong>mail :</strong>  iledelimpression@orange.fr
            </p>
          </div>
        </div>
        <div className="characterdiv">
          <h2>L'équipe</h2>
          <TestimonialsPage />
        </div>
        <footer className="mb-4">
        <div><a href="https://www.facebook.com/iledelimpression"><i class="fa fa-facebook-official"/></a></div>
        <p>Tous droits réservés - 2021 - île de l'impression. Reproductions des images et textes interdites.<br/>
        Designed & developped by <strong><a href="https://www.facebook.com/salmane.chahidi.56/">SC</a></strong></p>
        </footer>
      </div>
      
    );
  }
}