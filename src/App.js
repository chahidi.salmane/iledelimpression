import  { Component } from "react";
import { Navbar, Nav, Container } from 'react-bootstrap';
import { Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import "./App.css";
import AOS from 'aos';
import 'aos/dist/aos.css';
import Logo from './logo.jpeg';
import AuthService from "./services/auth.service";
import Login from "./components/login.component";
import Register from "./components/register.component";
import Contact from "./components/contact.component";
import Description from "./components/description.component";
import DescriptionImage from "./components/description-image.component";
import Presentation from "./components/presentation.component";
import Home from "./components/home.component";
import Profile from "./components/profile.component";
import BoardUser from "./components/board-user.component";
import BoardModerator from "./components/board-moderator.component";
import BoardAdmin from "./components/board-admin.component";
import Loader from "./components/spinner.component";
import BarMenu from "./components/barMenu.component";
import Message from "./components/message.component";
AOS.init();

class App extends Component {
  constructor(props) {
    super(props);
    this.logOut = this.logOut.bind(this);
    console.log(window.location.pathname);
    this.state = {
      showModeratorBoard: false,
      showAdminBoard: false,
      currentUser: undefined,
      loading: true,
      error: null,
      isLoaded: false,
      items: [],
    };
  }

  asyncCall() {
    return new Promise((resolve) => setTimeout(() => resolve(), 1000));
  }

  componentDidMount() {
    const user = AuthService.getCurrentUser();
    this.asyncCall().then(() => this.setState({ loading: false }));

        if (user) {
      this.setState({
        currentUser: user,
        showModeratorBoard: user.roles.includes("ROLE_MODERATOR"),
        showAdminBoard: user.roles.includes("ROLE_ADMIN"),
      });
    }
  }

  logOut() {
    AuthService.logout();
  }

  render() {
    const { currentUser, showModeratorBoard, showAdminBoard, error, loading } = this.state;
    if (error) {
      return <div>Erreur : {error.message}</div>;
    }else if(loading){
      return (
        <div>
        <BarMenu />
        <div class="spinner-grow text-primary" role="status">
          <span class="sr-only">Chargement...</span>
        </div>
        </div>);
      }   
    return (
      <div >
        <nav className="navbar navbar-expand navbar-light bg-white" id="administration-bar">
          <div className="navbar-nav mr-auto">

            {showModeratorBoard && (
              <li className="nav-item">
                <Link to={"/mod"} className="nav-link">
                  Moderator Board
                </Link>
              </li>
            )}

            {showAdminBoard && (
              <li className="nav-item">
                <Link to={"/admin"} className="nav-link">
                  Admin Board
                </Link>
              </li>
            )}

            {currentUser && (
              <li className="nav-item">
                <Link to={"/user"} className="nav-link">
                  Utilisateur
                </Link>
              </li>
            )}
          </div>

          {currentUser ? (
            <div className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to={"/profile"} className="nav-link">
                  {currentUser.username}
                </Link>
              </li>
              <li className="nav-item">
                <a href="/login" className="nav-link" onClick={this.logOut}>
                  Se déconnecter
                </a>
              </li>
            </div>
          ) : (
            <div className="navbar-nav ml-auto">
              <li className="nav-item">
                <Link to={"/login"} className="nav-link" id="login">
                  Se connecter
                </Link>
              </li>

              <li className="nav-item">
                <Link to={"/register"} className="nav-link" id="register">
                  S'inscrire
                </Link>
              </li>
            </div>
          )}
        </nav>
        <div className="container mt-3 ">
        <div className="center-navbar" >
        <Navbar fixed="top" expand="md">
              <Container>
                  <Navbar.Toggle aria-controls='responsive-navbar-nav' />
                  <Navbar.Collapse id='responsive-navbar-nav'>
                    <Nav fixed="top" activeKey={window.location.pathname} >
                      <Nav.Link  id="menu" href='/' >Accueil</Nav.Link>
                      <Nav.Link id="menu" href='/description' >Qui sommes-nous?</Nav.Link>
                      <Nav.Link href='/'><img src={Logo} id="logohome" alt = "image1"/></Nav.Link>
                      <Nav.Link id="menu" href='/presentation' >Présentation</Nav.Link>
                      <Nav.Link id="menu" href='/contact' >Contact</Nav.Link>
                    </Nav>
                  </Navbar.Collapse>
              </Container>
        </Navbar>
        </div>
          <Switch>
            <Route exact path={"/"} component={Home} />
            <Route exact path="/contact" component={Contact} />
            <Route exact path="/valider" component={Message} />
            <Route exact path="/spinner" component={Loader} />
            <Route exact path="/description" component={Description} />
            <Route exact path="/description-image" component={DescriptionImage} />
            <Route exact path="/presentation" component={Presentation} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/profile" component={Profile} />
            <Route path="/user" component={BoardUser} />
            <Route path="/mod" component={BoardModerator} />
            <Route path="/admin" component={BoardAdmin} />
          </Switch>
        </div>
        
      </div>
    );
  }
}

export default App;